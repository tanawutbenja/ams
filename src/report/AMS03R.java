package report;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams03rSearch", "/ams03rAdd", "/ams03rEdit", "/ams03rDelete" })
@MultipartConfig
public class AMS03R extends Abstract {
	private static final long serialVersionUID = 1L;

	private Map<String, String> softwareList;

	public AMS03R() {
		super();
		tableName = "";
		keyColumn = Arrays.asList("");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams03rSearch".equals(contentPath)) {
			sql = new StringBuilder();
			params = new ArrayList<>();
			bindingCriteria(request);
			sql.append(" SELECT LI.*, ASS.SOFTWARE_NAME, ASH.HARDWARE_NAME, AU.NAME, CASE WHEN TRUNC(LI.EXP_DATE) - TRUNC(LI.PURCH_DATE) > 0 THEN TRUNC(LI.EXP_DATE) - TRUNC(LI.PURCH_DATE) ELSE 0 END REMAIN FROM LICENSE LI ");
			sql.append(" LEFT JOIN ASSET_SOFTWARE ASS ");
			sql.append(" ON ASS.SOFTWARE_ID = LI.SOFTWARE_ID ");
			sql.append(" LEFT JOIN ASSET_HARDWARE ASH ");
			sql.append(" ON ASH.HARDWARE_ID = LI.HARDWARE_ID ");
			sql.append(" LEFT JOIN AUTHORITY AU ");
			sql.append(" ON AU.ID = ASH.EMP_ID ");
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("SOFTWARE_ID"))) {
				sql.append(" AND SOFTWARE_ID LIKE ? ");
				params.add(criteria.get("SOFTWARE_ID"));
			}
			
			if (isNotEmpty(criteria.get("STATUS"))) {
				sql.append(" AND LICENSE_STATUS LIKE ? ");
				params.add(criteria.get("STATUS"));
			}

			sql.append(" ORDER BY LI.SOFTWARE_ID, LI.EXP_DATE, AU.NAME ");

			search(response);
		} else {
			response.getWriter().print("error");
		}
	}

//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		if (!isInSession(request, response))
//			return;
//		String contentPath = request.getServletPath();
//		bindingSelectedItem(request);
//		sql = new StringBuilder();
//		params = new ArrayList<>();
//		int rowUpdate = -1;
//		try {
//			if ("/ams03rAdd".equals(contentPath)) {
//				rowUpdate = insertData();
//			} else if ("/ams03rEdit".equals(contentPath)) {
//				rowUpdate = updateData();
//			} else if ("/ams03rDelete".equals(contentPath)) {
//				rowUpdate = deleteData();
//			} else {
//				response.getWriter().print("error");
//			}
//		} catch (Exception e) {
//			System.out.println(e);
//			response.getWriter().print(e);
//		}
//
//		if (rowUpdate >= 0) {
//			response.getWriter().print("success");
//		}
//
//	}

	public Map<String, String> getSoftwareList() {
		softwareList = getMappingDropDown("ASSET_SOFTWARE", "SOFTWARE_ID", "SOFTWARE_NAME");
		return softwareList;
	}

	public void setSoftwareList(Map<String, String> softwareList) {
		this.softwareList = softwareList;
	}

}
