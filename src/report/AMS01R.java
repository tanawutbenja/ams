package report;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams01rSearch", "/ams01rAdd", "/ams01rEdit", "/ams01rDelete" })
@MultipartConfig
public class AMS01R extends Abstract {
	private static final long serialVersionUID = 1L;

	private Map<String, String> softwareList;
	private Map<String, String> softwareTypeList;

	public AMS01R() {
		super();
		tableName = "INVENT_SOFTWARE";
		keyColumn = Arrays.asList("INVENT_SOFTWARE_ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams01rSearch".equals(contentPath)) {
			sql = new StringBuilder();
			sql.append(" INSERT INTO INVENT_SOFTWARE ");
			sql.append(" SELECT NULL, SYSDATE LAST_UPDATE_DATE,  ");
			sql.append("     COUNT(0) PHYSICAL_QTY,  ");
			sql.append("     SUM(CASE WHEN L.HARDWARE_ID IS NULL THEN 1 END) AVAILABLE_QTY,  ");
			sql.append("     S.SOFTWARE_ID  ");
			sql.append(" FROM ASSET_SOFTWARE S ");
			sql.append(" JOIN LICENSE L ");
			sql.append(" ON L.SOFTWARE_ID = S.SOFTWARE_ID ");
			sql.append(" GROUP BY S.SOFTWARE_ID ");
			
			try {
				executeUpdate("DELETE INVENT_SOFTWARE");
				executeUpdate(sql.toString());
			} catch (SQLException e) {
				System.out.println(e);
			}
			
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT * FROM ").append(tableName);
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("SOFTWARE_ID"))) {
				sql.append(" AND SOFTWARE_ID LIKE ? ");
				params.add(criteria.get("SOFTWARE_ID"));
			}

			if (isNotEmpty(criteria.get("SOFTWARE_TYPE_ID"))) {
				sql.append(" AND SOFTWARE_TYPE_ID LIKE ? ");
				params.add(criteria.get("SOFTWARE_TYPE_ID"));
			}

			sql.append(" ORDER BY SOFTWARE_ID ");

			search(response);
		} else {
			response.getWriter().print("error");
		}
	}

//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		if (!isInSession(request, response))
//			return;
//		String contentPath = request.getServletPath();
//		bindingSelectedItem(request);
//		sql = new StringBuilder();
//		params = new ArrayList<>();
//		int rowUpdate = -1;
//		try {
//			if ("/ams01rAdd".equals(contentPath)) {
//				rowUpdate = insertData();
//			} else if ("/ams01rEdit".equals(contentPath)) {
//				rowUpdate = updateData();
//			} else if ("/ams01rDelete".equals(contentPath)) {
//				rowUpdate = deleteData();
//			} else {
//				response.getWriter().print("error");
//			}
//		} catch (Exception e) {
//			System.out.println(e);
//			response.getWriter().print(e);
//		}
//
//		if (rowUpdate >= 0) {
//			response.getWriter().print("success");
//		}
//
//	}

	public Map<String, String> getSoftwareList() {
		softwareList = getMappingDropDown("ASSET_SOFTWARE", "SOFTWARE_ID", "SOFTWARE_NAME");
		return softwareList;
	}

	public void setSoftwareList(Map<String, String> softwareList) {
		this.softwareList = softwareList;
	}
	
	public Map<String, String> getSoftwareTypeList() {
		softwareTypeList = getMappingDropDown("SOFTWARE_TYPE", "SOFTWARE_TYPE_ID", "SOFTWARE_TYPE_NAME");
		return softwareTypeList;
	}

	public void setSoftwareTypeList(Map<String, String> softwareTypeList) {
		this.softwareTypeList = softwareTypeList;
	}

}
