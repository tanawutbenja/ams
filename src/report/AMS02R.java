package report;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams02rSearch", "/ams02rAdd", "/ams02rEdit", "/ams02rDelete" })
@MultipartConfig
public class AMS02R extends Abstract {
	private static final long serialVersionUID = 1L;

	private Map<String, String> hardwareCategoryList;

	public AMS02R() {
		super();
		tableName = "INVENT_HARDWARE";
		keyColumn = Arrays.asList("INVENT_HARDWARE_ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams02rSearch".equals(contentPath)) {
			sql = new StringBuilder();
			sql.append(" INSERT INTO INVENT_HARDWARE ");
			sql.append(" SELECT NULL, SYSDATE LAST_UPDATE_DATE,  ");
			sql.append("     COUNT(0) PHYSICAL_QTY,  ");
			sql.append("     SUM(CASE WHEN EMP_ID IS NULL THEN 1 END) AVAILABLE_QTY,  ");
			sql.append("     HW_CATEGORY_ID ");
			sql.append(" FROM ASSET_HARDWARE ");
			sql.append(" GROUP BY HW_CATEGORY_ID ");
			
			try {
				executeUpdate("DELETE INVENT_HARDWARE");
				executeUpdate(sql.toString());
			} catch (SQLException e) {
				System.out.println(e);
			}
			
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT * FROM ").append(tableName);
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("HW_CATEGORY_ID"))) {
				sql.append(" AND HW_CATEGORY_ID LIKE ? ");
				params.add(criteria.get("HW_CATEGORY_ID "));
			}

			sql.append(" ORDER BY HW_CATEGORY_ID ");

			search(response);
		} else {
			response.getWriter().print("error");
		}
	}

//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		if (!isInSession(request, response))
//			return;
//		String contentPath = request.getServletPath();
//		bindingSelectedItem(request);
//		sql = new StringBuilder();
//		params = new ArrayList<>();
//		int rowUpdate = -1;
//		try {
//			if ("/ams02rAdd".equals(contentPath)) {
//				rowUpdate = insertData();
//			} else if ("/ams02rEdit".equals(contentPath)) {
//				rowUpdate = updateData();
//			} else if ("/ams02rDelete".equals(contentPath)) {
//				rowUpdate = deleteData();
//			} else {
//				response.getWriter().print("error");
//			}
//		} catch (Exception e) {
//			System.out.println(e);
//			response.getWriter().print(e);
//		}
//
//		if (rowUpdate >= 0) {
//			response.getWriter().print("success");
//		}
//
//	}

	public Map<String, String> getHardwareCategoryList() {
		hardwareCategoryList = getMappingDropDown("HARDWARE_CATEGORY", "ID", "CATEGORY_NAME");
		return hardwareCategoryList;
	}

	public void setHardwareCategoryList(Map<String, String> hardwareCategoryList) {
		this.hardwareCategoryList = hardwareCategoryList;
	}

}
