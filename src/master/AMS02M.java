package master;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams02mSearch", "/ams02mAdd", "/ams02mEdit", "/ams02mDelete" })
@MultipartConfig
public class AMS02M extends Abstract {
	private static final long serialVersionUID = 1L;

	private Map<String, String> softwareTypeList;

	public AMS02M() {
		super();
		tableName = "ASSET_SOFTWARE";
		keyColumn = Arrays.asList("SOFTWARE_ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams02mSearch".equals(contentPath)) {
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT * FROM ").append(tableName);
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("SOFTWARE_ID"))) {
				sql.append(" AND SOFTWARE_ID LIKE ? ");
				params.add(criteria.get("SOFTWARE_ID"));
			}

			if (isNotEmpty(criteria.get("SOFTWARE_NAME"))) {
				sql.append(" AND SOFTWARE_NAME LIKE ? ");
				params.add(criteria.get("SOFTWARE_NAME"));
			}

			if (isNotEmpty(criteria.get("SOFTWARE_TYPE_ID"))) {
				sql.append(" AND SOFTWARE_TYPE_ID LIKE ? ");
				params.add(criteria.get("SOFTWARE_TYPE_ID"));
			}

			sql.append(" ORDER BY SOFTWARE_ID ");

			search(response);
		} else {
			response.getWriter().print("error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		bindingSelectedItem(request);
		sql = new StringBuilder();
		params = new ArrayList<>();
		int rowUpdate = -1;
		try {
			if ("/ams02mAdd".equals(contentPath)) {
				rowUpdate = insertData();
			} else if ("/ams02mEdit".equals(contentPath)) {
				rowUpdate = updateData();
			} else if ("/ams02mDelete".equals(contentPath)) {
				rowUpdate = deleteData();
			} else {
				response.getWriter().print("error");
			}
		} catch (Exception e) {
			System.out.println(e);
			response.getWriter().print(e);
		}

		if (rowUpdate >= 0) {
			response.getWriter().print("success");
		}

	}

	public Map<String, String> getSoftwareTypeList() {
		softwareTypeList = getMappingDropDown("SOFTWARE_TYPE", "SOFTWARE_TYPE_ID", "SOFTWARE_TYPE_NAME");
		return softwareTypeList;
	}

	public void setSoftwareTypeList(Map<String, String> softwareTypeList) {
		this.softwareTypeList = softwareTypeList;
	}

}
