package master;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams08mSearch", "/ams08mAdd", "/ams08mEdit", "/ams08mDelete" })
@MultipartConfig
public class AMS08M extends Abstract {
	private static final long serialVersionUID = 1L;

	public AMS08M() {
		super();
		tableName = "SERVICE_TYPE";
		keyColumn = Arrays.asList("SERVICE_TYPE_ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams08mSearch".equals(contentPath)) {
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT * FROM ").append(tableName);
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("SERVICE_TYPE_ID"))) {
				sql.append(" AND SERVICE_TYPE_ID LIKE ? ");
				params.add(criteria.get("SERVICE_TYPE_ID"));
			}

			if (isNotEmpty(criteria.get("SERVICE_TYPE_NAME"))) {
				sql.append(" AND SERVICE_TYPE_NAME LIKE ? ");
				params.add(criteria.get("SERVICE_TYPE_NAME"));
			}

			sql.append(" ORDER BY SERVICE_TYPE_ID ");

			search(response);
		} else {
			response.getWriter().print("error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		bindingSelectedItem(request);
		sql = new StringBuilder();
		params = new ArrayList<>();
		int rowUpdate = -1;
		try {
			if ("/ams08mAdd".equals(contentPath)) {
				rowUpdate = insertData();
			} else if ("/ams08mEdit".equals(contentPath)) {
				rowUpdate = updateData();
			} else if ("/ams08mDelete".equals(contentPath)) {
				rowUpdate = deleteData();
			} else {
				response.getWriter().print("error");
			}
		} catch (Exception e) {
			System.out.println(e);
			response.getWriter().print(e);
		}

		if (rowUpdate >= 0) {
			response.getWriter().print("success");
		}

	}

}
