package master;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams07mSearch", "/ams07mAdd", "/ams07mEdit", "/ams07mDelete", "/ams07mEmployee" })
@MultipartConfig
public class AMS07M extends Abstract {
	private static final long serialVersionUID = 1L;

	private Map<String, String> hardwareCategoryList;
	private Map<String, String> employeeList;

	public AMS07M() {
		super();
		tableName = "ASSET_HARDWARE";
		keyColumn = Arrays.asList("HARDWARE_ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams07mSearch".equals(contentPath)) {
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT A.*, B.NAME EMP_NAME FROM ").append(tableName).append(" A ");
			sql.append(" LEFT JOIN AUTHORITY B ");
			sql.append(" ON B.ID = A.EMP_ID ");
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("HARDWARE_ID"))) {
				sql.append(" AND HARDWARE_ID LIKE ? ");
				params.add(criteria.get("HARDWARE_ID"));
			}

			if (isNotEmpty(criteria.get("HARDWARE_NAME"))) {
				sql.append(" AND HARDWARE_NAME LIKE ? ");
				params.add(criteria.get("HARDWARE_NAME"));
			}

			if (isNotEmpty(criteria.get("EMP_NAME"))) {
				if ("-".equals(criteria.get("EMP_NAME").toString())) {
					sql.append(" AND B.NAME IS NULL ");
				} else {
					sql.append(" AND UPPER(B.NAME) LIKE UPPER(?) ");
					params.add(criteria.get("EMP_NAME"));
				}
			}

			if (isNotEmpty(criteria.get("HW_CATEGORY_ID"))) {
				sql.append(" AND HW_CATEGORY_ID LIKE ? ");
				params.add(criteria.get("HW_CATEGORY_ID"));
			}

			sql.append(" ORDER BY HARDWARE_ID ");

			search(response);
		} else if ("/ams07mEmployee".equals(contentPath)) {
			response.setContentType("application/json;charset=UTF-8");
			Gson gson = new GsonBuilder().create();
			response.getWriter().print(gson.toJson(getEmployeeList().values()));
		} else {
			response.getWriter().print("error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		bindingSelectedItem(request);
		sql = new StringBuilder();
		params = new ArrayList<>();
		int rowUpdate = -1;
		try {
			if ("/ams07mAdd".equals(contentPath)) {
				if (selectedItem.get("EMP_NAME") != null && selectedItem.get("EMP_NAME").toString().length() > 0) {
					selectedItem.put("EMP_ID", getEmployeeId(selectedItem.get("EMP_NAME").toString()));
					if ((int) selectedItem.get("EMP_ID") < 0) {
						response.getWriter().print("Employee Name " + selectedItem.get("EMP_NAME").toString() + " is not exists");
						return;
					}
				}
				selectedItem.remove("EMP_NAME");
				rowUpdate = insertData();
			} else if ("/ams07mEdit".equals(contentPath)) {
				if (selectedItem.get("EMP_NAME") != null && selectedItem.get("EMP_NAME").toString().length() > 0) {
					selectedItem.put("EMP_ID", getEmployeeId(selectedItem.get("EMP_NAME").toString()));
					if ((int) selectedItem.get("EMP_ID") < 0) {
						response.getWriter().print("Employee Name " + selectedItem.get("EMP_NAME").toString() + " is not exists");
						return;
					}
				}
				selectedItem.remove("EMP_NAME");
				rowUpdate = updateData();
			} else if ("/ams07mDelete".equals(contentPath)) {
				rowUpdate = deleteData();
			} else {
				response.getWriter().print("error");
			}
		} catch (Exception e) {
			System.out.println(e);
			response.getWriter().print(e);
		}

		if (rowUpdate >= 0) {
			response.getWriter().print("success");
		}

	}

	@SuppressWarnings("null")
	private int getEmployeeId(String empName) {
		Map<String, Object> result = querySingleRow("SELECT ID FROM AUTHORITY WHERE UPPER(NAME) = UPPER(?)", Arrays.asList(empName));
		return result != null || result.size() > 0 ? Integer.parseInt(result.get("ID").toString()) : -1;
	}

	public Map<String, String> getHardwareCategoryList() {
		hardwareCategoryList = getMappingDropDown("HARDWARE_CATEGORY", "ID", "CATEGORY_NAME");
		return hardwareCategoryList;
	}

	public void setHardwareCategoryList(Map<String, String> hardwareCategoryList) {
		this.hardwareCategoryList = hardwareCategoryList;
	}

	public Map<String, String> getEmployeeList() {
		employeeList = getMappingDropDown("AUTHORITY", "ID", "NAME");
		return employeeList;
	}

	public void setEmployeeList(Map<String, String> employeeList) {
		this.employeeList = employeeList;
	}

}
