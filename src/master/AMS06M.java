package master;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams06mSearch", "/ams06mAdd", "/ams06mEdit", "/ams06mDelete" })
@MultipartConfig
public class AMS06M extends Abstract {
	private static final long serialVersionUID = 1L;

	public AMS06M() {
		super();
		tableName = "HARDWARE_CATEGORY";
		keyColumn = Arrays.asList("ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams06mSearch".equals(contentPath)) {
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT * FROM ").append(tableName);
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("ID"))) {
				sql.append(" AND ID LIKE ? ");
				params.add(criteria.get("ID"));
			}

			if (isNotEmpty(criteria.get("CATEGORY_NAME"))) {
				sql.append(" AND CATEGORY_NAME LIKE ? ");
				params.add(criteria.get("CATEGORY_NAME"));
			}

			sql.append(" ORDER BY ID ");

			search(response);
		} else {
			response.getWriter().print("error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		bindingSelectedItem(request);
		sql = new StringBuilder();
		params = new ArrayList<>();
		int rowUpdate = -1;
		try {
			if ("/ams06mAdd".equals(contentPath)) {
				rowUpdate = insertData();
			} else if ("/ams06mEdit".equals(contentPath)) {
				rowUpdate = updateData();
			} else if ("/ams06mDelete".equals(contentPath)) {
				rowUpdate = deleteData();
			} else {
				response.getWriter().print("error");
			}
		} catch (Exception e) {
			System.out.println(e);
			response.getWriter().print(e);
		}

		if (rowUpdate >= 0) {
			response.getWriter().print("success");
		}

	}

}
