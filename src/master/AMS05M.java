package master;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams05mSearch", "/ams05mAdd", "/ams05mEdit", "/ams05mDelete" })
@MultipartConfig
public class AMS05M extends Abstract {
	private static final long serialVersionUID = 1L;

	private Map<String, String> departmentList;
	private Map<String, String> positionList;

	public AMS05M() {
		super();
		tableName = "AUTHORITY";
		keyColumn = Arrays.asList("ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams05mSearch".equals(contentPath)) {
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT ID, USERNAME, SUBSTR(PASSWORD, 1, 16) PASSWORD, SUBSTR(PASSWORD, 1, 16) CONFIRM_PASSWORD, NAME, POSITION, DEPARTMENT_ID FROM ").append(tableName);
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("NAME"))) {
				sql.append(" AND NAME LIKE ? ");
				params.add(criteria.get("NAME"));
			}

			if (isNotEmpty(criteria.get("POSITION"))) {
				sql.append(" AND POSITION LIKE ? ");
				params.add(criteria.get("POSITION"));
			}

			if (isNotEmpty(criteria.get("DEPARTMENT_ID"))) {
				sql.append(" AND DEPARTMENT_ID LIKE ? ");
				params.add(criteria.get("DEPARTMENT_ID"));
			}

			sql.append(" ORDER BY ID ");

			search(response);
		} else {
			response.getWriter().print("error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		bindingSelectedItem(request);
		sql = new StringBuilder();
		params = new ArrayList<>();
		int rowUpdate = -1;
		try {
			if ("/ams05mAdd".equals(contentPath)) {
				selectedItem.remove("CONFIRM_PASSWORD");
				selectedItem.put("PASSWORD", hashPassword(selectedItem.get("PASSWORD").toString()));
				rowUpdate = insertData();
			} else if ("/ams05mEdit".equals(contentPath)) {
				selectedItem.remove("CONFIRM_PASSWORD");
				Map<String, Object> previousItem = dataList.stream().filter(o -> o.get("ID").equals(selectedItem.get("ID"))).collect(Collectors.toList()).get(0);
				if (previousItem.get("PASSWORD").equals(selectedItem.get("PASSWORD"))) {
					selectedItem.put("PASSWORD", hashPassword(selectedItem.get("PASSWORD").toString()));
				}
				rowUpdate = updateData();
			} else if ("/ams05mDelete".equals(contentPath)) {
				rowUpdate = deleteData();
			} else {
				response.getWriter().print("error");
			}
		} catch (Exception e) {
			System.out.println(e);
			response.getWriter().print(e);
		}

		if (rowUpdate >= 0) {
			response.getWriter().print("success");
		}

	}

	public Map<String, String> getDepartmentList() {
		departmentList = getMappingDropDown("DEPARTMENT", "DEPARTMENT_ID", "DEPARTMENT_NAME");
		return departmentList;
	}

	public void setDepartmentList(Map<String, String> departmentList) {
		this.departmentList = departmentList;
	}

	public Map<String, String> getPositionList() {
		positionList = getMappingDropDown("POSITION", "POSITION_ID", "POSITION_NAME");
		return positionList;
	}

	public void setPositionList(Map<String, String> positionList) {
		this.positionList = positionList;
	}

}
