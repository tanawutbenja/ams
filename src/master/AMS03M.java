package master;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.reflect.TypeToken;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams03mSearch", "/ams03mSearchDetail", "/ams03mAdd", "/ams03mEdit", "/ams03mDelete" })
@MultipartConfig
public class AMS03M extends Abstract {
	private static final long serialVersionUID = 1L;

	public AMS03M() {
		super();
		tableName = "DEPARTMENT";
		keyColumn = Arrays.asList("DEPARTMENT_ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams03mSearch".equals(contentPath)) {
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT * FROM ").append(tableName);
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("DEPARTMENT_ID"))) {
				sql.append(" AND DEPARTMENT_ID LIKE ? ");
				params.add(criteria.get("DEPARTMENT_ID"));
			}

			if (isNotEmpty(criteria.get("DEPARTMENT_NAME"))) {
				sql.append(" AND DEPARTMENT_NAME LIKE ? ");
				params.add(criteria.get("DEPARTMENT_NAME"));
			}

			sql.append(" ORDER BY DEPARTMENT_ID ");

			search(response);
		} else if ("/ams03mSearchDetail".equals(contentPath)) {
			List<Map<String, Object>> details = gson.fromJson(request.getParameter("data"), new TypeToken<ArrayList<Map<String, Object>>>() {
			}.getType());
			System.out.println(details);
		} else {
			response.getWriter().print("error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		bindingSelectedItem(request);
		sql = new StringBuilder();
		params = new ArrayList<>();
		int rowUpdate = -1;
		try {
			if ("/ams03mAdd".equals(contentPath)) {
				rowUpdate = insertData();
			} else if ("/ams03mEdit".equals(contentPath)) {
				rowUpdate = updateData();
			} else if ("/ams03mDelete".equals(contentPath)) {
				rowUpdate = deleteData();
			} else {
				response.getWriter().print("error");
			}
		} catch (Exception e) {
			System.out.println(e);
			response.getWriter().print(e);
		}

		if (rowUpdate >= 0) {
			response.getWriter().print("success");
		}

	}

}
