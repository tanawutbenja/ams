package process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams01pSearch", "/ams01pAdd", "/ams01pEdit", "/ams01pDelete", "/ams01pHardware" })
@MultipartConfig
public class AMS01P extends Abstract {
	private static final long serialVersionUID = 1L;

	private Map<String, String> softwareList;

	public AMS01P() {
		super();
		tableName = "LICENSE";
		keyColumn = Arrays.asList("LICENSE_ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams01pSearch".equals(contentPath)) {
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT A.*, B.HARDWARE_NAME HARDWARE FROM ").append(tableName).append(" A ");
			sql.append(" LEFT JOIN ASSET_HARDWARE B ");
			sql.append(" ON B.HARDWARE_ID = A.HARDWARE_ID ");
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("SOFTWARE_ID"))) {
				sql.append(" AND A.SOFTWARE_ID = ? ");
				params.add(criteria.get("SOFTWARE_ID"));
			}

			sql.append(" ORDER BY A.LICENSE_ID ");

			search(response);
		} else if ("/ams01pHardware".equals(contentPath)) {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT A.HARDWARE_NAME||CASE WHEN B.NAME IS NULL THEN '' ELSE ' : '||B.NAME END HARDWARE ");
			sql.append(" FROM ASSET_HARDWARE A   ");
			sql.append(" LEFT JOIN AUTHORITY B   ");
			sql.append(" ON B.ID = A.EMP_ID   ");
			sql.append(" WHERE A.HW_CATEGORY_ID = 1 ");
			sql.append(" ORDER BY HARDWARE_NAME ");

			List<Map<String, Object>> results = query(sql.toString());
			response.setContentType("application/json;charset=UTF-8");
			Gson gson = new GsonBuilder().create();
			response.getWriter().print(gson.toJson(results.stream().map(o -> o.get("HARDWARE").toString()).collect(Collectors.toList())));
		} else {
			response.getWriter().print("error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		bindingSelectedItem(request);
		sql = new StringBuilder();
		params = new ArrayList<>();
		int rowUpdate = -1;
		try {
			if ("/ams01pAdd".equals(contentPath)) {
				if (selectedItem.get("HARDWARE") != null && selectedItem.get("HARDWARE").toString().length() > 0) {
					selectedItem.put("HARDWARE_ID", getHardwareId(selectedItem.get("HARDWARE").toString()));
					if ((int) selectedItem.get("HARDWARE_ID") < 0) {
						response.getWriter().print("Hardware " + selectedItem.get("HARDWARE").toString() + " is not exists");
						return;
					}
				}
				selectedItem.remove("HARDWARE");
				selectedItem.put("LICENSE_STATUS", selectedItem.get("HARDWARE_ID") != null ? "N" : "Y");
				rowUpdate = insertData();
			} else if ("/ams01pEdit".equals(contentPath)) {
				if (selectedItem.get("HARDWARE") != null && selectedItem.get("HARDWARE").toString().length() > 0) {
					selectedItem.put("HARDWARE_ID", getHardwareId(selectedItem.get("HARDWARE").toString()));
					if ((int) selectedItem.get("HARDWARE_ID") < 0) {
						response.getWriter().print("Hardware " + selectedItem.get("HARDWARE").toString() + " is not exists");
						return;
					}
				}
				selectedItem.remove("HARDWARE");
				selectedItem.put("LICENSE_STATUS", selectedItem.get("HARDWARE_ID") != null ? "N" : "Y");
				rowUpdate = updateData();
			} else if ("/ams01pDelete".equals(contentPath)) {
				rowUpdate = deleteData();
			} else {
				response.getWriter().print("error");
			}
		} catch (Exception e) {
			System.out.println(e);
			response.getWriter().print(e);
		}

		if (rowUpdate >= 0) {
			response.getWriter().print("success");
		}

	}

	@SuppressWarnings("null")
	private int getHardwareId(String hardwareName) {
		Map<String, Object> result = querySingleRow("SELECT HARDWARE_ID FROM ASSET_HARDWARE WHERE UPPER(HARDWARE_NAME) = UPPER(?)", Arrays.asList(hardwareName));
		return result != null || result.size() > 0 ? Integer.parseInt(result.get("HARDWARE_ID").toString()) : -1;
	}

	public Map<String, String> getSoftwareList() {
		softwareList = getMappingDropDown("ASSET_SOFTWARE", "SOFTWARE_ID", "SOFTWARE_NAME");
		return softwareList;
	}

	public void setSoftwareList(Map<String, String> softwareList) {
		this.softwareList = softwareList;
	}

}
