package process;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import common.Abstract;

@WebServlet(urlPatterns = { "/ams03pSearch", "/ams03pSearchDetail", "/ams03pApprove", "/ams03pReject", "/ams03pDeleteDetail", "/ams03pEmployee" })
@MultipartConfig
public class AMS03P extends Abstract {
	private static final long serialVersionUID = 1L;

	private Map<String, String> serviceTypeList;
	private Map<String, String> employeeList;
	private Map<String, String> hardwareCategoryList;
	private Map<String, String> softwareList;

	private List<Map<String, Object>> dataDetailList;

	public AMS03P() {
		super();
		tableName = "REQUEST_ORDER";
		keyColumn = Arrays.asList("REQ_ORDER_ID");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		if ("/ams03pSearch".equals(contentPath)) {
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT * FROM ").append(tableName).append(" A ");
			sql.append(" WHERE 1=1 ");

			if (isNotEmpty(criteria.get("SERVICE_TYPE_ID"))) {
				sql.append(" AND A.SERVICE_TYPE_ID = ? ");
				params.add(criteria.get("SERVICE_TYPE_ID"));
			}

			sql.append(" ORDER BY A.REQ_ORDER_ID ");

			search(response);
		} else if ("/ams03pSearchDetail".equals(contentPath)) {
			int reqId = Integer.parseInt(request.getParameter("reqId"));
			sql = new StringBuilder();
			params = new ArrayList<>();

			bindingCriteria(request);
			sql.append(" SELECT A.*, A.REQ_ORDER_TYPE||':'||A.REQ_ORDER_TYPE_ID AS REQ_ORDER_TYPE_ID_COL FROM ").append("REQUEST_ORDER_LINE").append(" A ");
			sql.append(" WHERE REQ_ORDER_ID = ? ");
			sql.append(" ORDER BY A.REQ_ORDER_TYPE, A.REQ_ORDER_TYPE_ID ");
			params.add(reqId);

			try {
				setDataDetailList(query(sql.toString(), params));
				response.setContentType("application/json;charset=UTF-8");

				ServletOutputStream out = response.getOutputStream();
				String output = gson.toJson(dataDetailList);
				out.print(output);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if ("/ams03pEmployee".equals(contentPath)) {
			response.setContentType("application/json;charset=UTF-8");
			Gson gson = new GsonBuilder().create();
			response.getWriter().print(gson.toJson(getEmployeeList().values()));
		} else {
			response.getWriter().print("error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!isInSession(request, response))
			return;
		String contentPath = request.getServletPath();
		// bindingSelectedItem(request);
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<>();
		int rowUpdate = 0;
		try {
//			if ("/ams03pAdd".equals(contentPath)) {
//				Map<String, Object> form = gson.fromJson(request.getParameter("header"), new TypeToken<Map<String, Object>>() {
//				}.getType());
//				List<Map<String, Object>> detail = gson.fromJson(request.getParameter("list").toString(), new TypeToken<ArrayList<Map<String, Object>>>() {
//				}.getType());
//
//				sql.append(" INSERT INTO REQUEST_ORDER (DESCRIPTION, CREATED_BY, CREATED_DATE, SERVICE_TYPE_ID, ASSIGN_TO) ");
//				sql.append(" VALUES (?, ?, SYSDATE, ?, ?) ");
//				params.add(form.get("DESCRIPTION"));
//				params.add(getEmployeeId(userInfo.get("NAME").toString()));
//				params.add(form.get("SERVICE_TYPE_ID"));
//				params.add(getEmployeeId(form.get("ASSIGN_TO").toString()));
//
//				rowUpdate = executeUpdate(sql.toString(), params);
//				int requestId = getNewRequestId();
//
//				sql = new StringBuilder();
//				sql.append(" INSERT INTO REQUEST_ORDER_LINE (REQ_ORDER_ID, DESCRIPTION, REQ_ORDER_TYPE, REQ_ORDER_TYPE_ID, STATUS, UPDATED_BY, UPDATED_DATE) ");
//				sql.append(" VALUES (?, ?, ?, ?, ?, ?, SYSDATE) ");
//				for (Map<String, Object> det : detail) {
//					params = new ArrayList<>();
//					params.add(requestId);
//					params.add(det.get("DESCRIPTION"));
//					params.add(det.get("REQ_ORDER_TYPE"));
//					params.add(det.get("REQ_ORDER_TYPE_ID"));
//					params.add("Pending");
//					params.add(getEmployeeId(userInfo.get("NAME").toString()));
//
//					rowUpdate = executeUpdate(sql.toString(), params);
//				}
//			} else if ("/ams03pEdit".equals(contentPath)) {
//				Map<String, Object> form = gson.fromJson(request.getParameter("header"), new TypeToken<Map<String, Object>>() {
//				}.getType());
//				List<Map<String, Object>> detail = gson.fromJson(request.getParameter("list").toString(), new TypeToken<ArrayList<Map<String, Object>>>() {
//				}.getType());
//
//				List<Integer> reqLineNumber = detail.stream().map(o -> (new BigDecimal(o.get("REQ_LINE_NUMBER").toString()).intValue())).collect(Collectors.toList());
//				sql = new StringBuilder();
//				sql.append(" DELETE REQUEST_ORDER_LINE WHERE REQ_LINE_NUMBER = ? ");
//				for (Map<String, Object> delete : dataDetailList) {
//					if (!reqLineNumber.contains(Integer.parseInt(delete.get("REQ_LINE_NUMBER").toString()))) {
//
//						rowUpdate = executeUpdate(sql.toString(), Arrays.asList(delete.get("REQ_LINE_NUMBER")));
//					}
//				}
//			} else if ("/ams03pDelete".equals(contentPath)) {
//				Map<String, Object> form = gson.fromJson(request.getParameter("header"), new TypeToken<Map<String, Object>>() {
//				}.getType());
//
//				sql.append(" DELETE REQUEST_ORDER_LINE WHERE REQ_ORDER_ID = ? ");
//				rowUpdate = executeUpdate(sql.toString(), Arrays.asList(form.get("REQ_ORDER_ID")));
//
//				sql = new StringBuilder();
//				sql.append(" DELETE REQUEST_ORDER WHERE REQ_ORDER_ID = ? ");
//				rowUpdate = executeUpdate(sql.toString(), Arrays.asList(form.get("REQ_ORDER_ID")));
			if ("/ams03pApprove".equals(contentPath)) {
				Map<String, Object> form = gson.fromJson(request.getParameter("header"), new TypeToken<Map<String, Object>>() {
				}.getType());
				List<Map<String, Object>> detail = gson.fromJson(request.getParameter("list").toString(), new TypeToken<ArrayList<Map<String, Object>>>() {
				}.getType());
				
				rowUpdate = executeUpdate(" UPDATE REQUEST_ORDER SET STATUS = 'A' WHERE REQ_ORDER_ID = ? ", Arrays.asList(form.get("REQ_ORDER_ID")));
			} else if ("/ams03pReject".equals(contentPath)) {
				Map<String, Object> form = gson.fromJson(request.getParameter("header"), new TypeToken<Map<String, Object>>() {
				}.getType());
				List<Map<String, Object>> detail = gson.fromJson(request.getParameter("list").toString(), new TypeToken<ArrayList<Map<String, Object>>>() {
				}.getType());
				
				rowUpdate = executeUpdate(" UPDATE REQUEST_ORDER SET STATUS = 'R' WHERE REQ_ORDER_ID = ? ", Arrays.asList(form.get("REQ_ORDER_ID")));
			} else {
				response.getWriter().print("error");
			}
		} catch (Exception e) {
			System.out.println(e);
			response.getWriter().print(e);
		}

		if (rowUpdate >= 0) {
			response.getWriter().print("success");
		}

	}

	@SuppressWarnings("null")
	private int getNewRequestId() {
		Map<String, Object> result = querySingleRow("SELECT REQ_ORDER_ID FROM REQUEST_ORDER WHERE CREATED_BY = ? ORDER BY CREATED_DATE DESC", Arrays.asList(getEmployeeId(userInfo.get("NAME").toString())));
		return result != null || result.size() > 0 ? Integer.parseInt(result.get("REQ_ORDER_ID").toString()) : -1;
	}

	@SuppressWarnings("null")
	private int getEmployeeId(String empName) {
		Map<String, Object> result = querySingleRow("SELECT ID FROM AUTHORITY WHERE UPPER(NAME) = UPPER(?)", Arrays.asList(empName));
		return result != null || result.size() > 0 ? Integer.parseInt(result.get("ID").toString()) : -1;
	}

	public Map<String, String> getServiceTypeList() {
		serviceTypeList = getMappingDropDown("SERVICE_TYPE", "SERVICE_TYPE_ID", "SERVICE_TYPE_NAME");
		return serviceTypeList;
	}

	public void setServiceTypeList(Map<String, String> serviceTypeList) {
		this.serviceTypeList = serviceTypeList;
	}

	public Map<String, String> getHardwareCategoryList() {
		hardwareCategoryList = getMappingDropDown("HARDWARE_CATEGORY", "ID", "CATEGORY_NAME");
		return hardwareCategoryList;
	}

	public void setHardwareCategoryList(Map<String, String> hardwareCategoryList) {
		this.hardwareCategoryList = hardwareCategoryList;
	}

	public Map<String, String> getSoftwareList() {
		softwareList = getMappingDropDown("ASSET_SOFTWARE", "SOFTWARE_ID", "SOFTWARE_NAME");
		return softwareList;
	}

	public void setSoftwareList(Map<String, String> softwareList) {
		this.softwareList = softwareList;
	}

	public Map<String, String> getEmployeeList() {
		employeeList = getMappingDropDown("AUTHORITY", "ID", "NAME");
		return employeeList;
	}

	public void setEmployeeList(Map<String, String> employeeList) {
		this.employeeList = employeeList;
	}

	public List<Map<String, Object>> getDataDetailList() {
		return dataDetailList;
	}

	public void setDataDetailList(List<Map<String, Object>> dataDetailList) {
		this.dataDetailList = dataDetailList;
	}

}
