package common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet(urlPatterns = { "/menu" })
@MultipartConfig
public class Main extends Database {
	private static final long serialVersionUID = -2818029287831621588L;

	protected String currentMenu = "common/begin.jsp";
	public List<Map<String, Object>> menu;
	public List<String> menuTag;

	public Main() {
		super();
		menu = new ArrayList<>();
		menuTag = new ArrayList<>();
	}

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("userInfo") != null) {

			menu = query("SELECT * FROM MENU WHERE POSITION = ? ORDER BY MENU_SEQ", Arrays.asList(((Map<String, Object>) session.getAttribute("userInfo")).get("POSITION")));
			menuTag.clear();
			menu.forEach(o -> menuTag.add("<a class=\"menu-item\" href=\"javascript:void(0)\" onClick=\"gotoPage('" + o.get("MENU_URL") + "')\">" + o.get("MENU_NAME") + "</a>"));
			response.setContentType("application/json;charset=UTF-8");
			Gson gson = new GsonBuilder().create();
			response.getWriter().print(gson.toJson(menuTag));
		} else {
			response.getWriter().print("error");
		}
	}

	public List<Map<String, Object>> getMenuList() {
		return menu;
	}

}
