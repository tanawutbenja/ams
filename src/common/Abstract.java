package common;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Abstract extends Database {
	private static final long serialVersionUID = -2818029287831621588L;

	private final String dateFormat = "yyyy-MM-dd";
	private SimpleDateFormat sdf;

	protected String currentMenu = "common/begin.jsp";
	protected List<Map<String, Object>> dataList;
	protected Map<String, Object> criteria;
	protected Map<String, Object> userInfo;
	protected Map<String, Object> selectedItem;
	protected StringBuilder sql;
	protected List<Object> params;
	protected String tableName;
	protected List<String> keyColumn;
	protected final Gson gson;

	public Abstract() {
		dataList = new ArrayList<>();
		criteria = new HashMap<>();
		selectedItem = new HashMap<>();
		sql = new StringBuilder();
		params = new ArrayList<>();
		gson = new GsonBuilder().create();
		tableName = "";
		keyColumn = new ArrayList<>();
		sdf = new SimpleDateFormat(dateFormat);
	}

	public void search(HttpServletResponse response) {
		try {
			setDataList(query(sql.toString(), params));
			response.setContentType("application/json;charset=UTF-8");

			ServletOutputStream out = response.getOutputStream();
			String output = gson.toJson(dataList);
			out.print(output);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int insertData() throws SQLException {
		sql = new StringBuilder();
		sql.append(" INSERT INTO ").append(tableName);
		Map<String, Object> insertItem = selectedItem.entrySet().stream().filter(o -> isNotEmpty(o.getValue())).collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
		sql.append(" ( ").append(insertItem.keySet().stream().map(o -> "\"" + o + "\"").collect(Collectors.joining(", "))).append(" ) ");
		sql.append(" VALUES ( ").append(insertItem.keySet().stream().map(o -> "?").collect(Collectors.joining(", "))).append(" ) ");
		insertItem.values().stream().forEach(o -> params.add(o));
		return executeUpdate(sql.toString(), params);
	}

	public int updateData() throws SQLException {
		sql = new StringBuilder();
		sql.append(" UPDATE ").append(tableName);
		Map<String, Object> filterItem = selectedItem.entrySet().stream().filter(o -> !keyColumn.contains(o.getKey()) && isNotEmpty(o.getValue())).collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
		sql.append(" SET ").append(filterItem.keySet().stream().map(o -> o + " = ?").collect(Collectors.joining(", ")));
		filterItem.values().forEach(o -> params.add(o));
		sql.append(" WHERE ").append(keyColumn.stream().map(o -> o + " = ?").collect(Collectors.joining(" AND ")));
		keyColumn.forEach(o -> params.add(selectedItem.get(o)));
		return executeUpdate(sql.toString(), params);
	}

	public int deleteData() throws SQLException {
		sql = new StringBuilder();
		sql.append(" DELETE ").append(tableName);
		sql.append(" WHERE ").append(keyColumn.stream().map(o -> o + " = ?").collect(Collectors.joining(" AND ")));
		keyColumn.forEach(o -> params.add(selectedItem.get(o)));
		return executeUpdate(sql.toString(), params);
	}

	@SuppressWarnings("unchecked")
	public boolean isInSession(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("userInfo") != null) {
			userInfo = (Map<String, Object>) session.getAttribute("userInfo");
			return true;
		} else {
			try {
				request.getRequestDispatcher("index.jsp").forward(request, response);
			} catch (ServletException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	public void bindingCriteria(HttpServletRequest request) {
		Enumeration<String> enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			String parameterName = enumeration.nextElement();
			criteria.put(parameterName, request.getParameter(parameterName));
		}
	}

	public void bindingSelectedItem(HttpServletRequest request) {
		Enumeration<String> enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			String parameterName = enumeration.nextElement();
			Object value = "";
			if (parameterName.contains("DATE")) {
				value = java.sql.Date.valueOf(request.getParameter(parameterName));
			} else {
				value = request.getParameter(parameterName);
			}
			selectedItem.put(parameterName, value);
		}
	}

	public void clear() {
		criteria.clear();
	}

	public Map<String, String> getMappingDropDown(String table, String columnKey, String columnValue) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT " + columnKey + " KEY, " + columnValue + " VALUE ");
		sql.append(" FROM " + table);
		sql.append(" GROUP BY " + columnKey + ", " + columnValue);
		sql.append(" ORDER BY " + columnValue);
		List<Map<String, Object>> results = query(sql.toString());
		Map<String, String> dropdrow = new HashMap<>();
		results.stream().forEachOrdered(o -> dropdrow.put(o.get("KEY").toString(), o.get("VALUE").toString()));
		return dropdrow;
	}

	public boolean isNotEmpty(Object obj) {
		return obj != null && obj.toString().trim().length() != 0;
	}

	public List<Map<String, Object>> getDataList() {
		return dataList;
	}

	public void setDataList(List<Map<String, Object>> dataList) {
		this.dataList = dataList;
	}

	public Map<String, Object> getCriteria() {
		return criteria;
	}

	public void setCriteria(Map<String, Object> criteria) {
		this.criteria = criteria;
	}

	public Map<String, Object> getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(Map<String, Object> userInfo) {
		this.userInfo = userInfo;
	}

}
