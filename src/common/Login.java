package common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet(urlPatterns = { "/login", "/logout" })
@MultipartConfig
public class Login extends Database {
	private static final long serialVersionUID = 1L;

	public Login() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session != null && session.getAttribute("userInfo") != null) {
			response.getWriter().print("success");
		} else {
			response.getWriter().print("error");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String contentPath = request.getServletPath();
		if ("/login".equals(contentPath)) {
			String username = request.getParameter("username").trim();
			String password = request.getParameter("password").trim();
			String myHash = hashPassword(password);

			StringBuilder sql = new StringBuilder();
			List<Object> param = new ArrayList<>();
			sql.append("SELECT * FROM AUTHORITY WHERE USERNAME = ? AND PASSWORD = ?");
			param.add(username);
			param.add(myHash);
			Map<String, Object> auth = querySingleRow(sql.toString(), param);
			response.setContentType("text/html;charset=UTF-8");
			if (auth != null) {
				session.setAttribute("userInfo", auth);
				session.setMaxInactiveInterval(1800);
				response.setContentType("application/json;charset=UTF-8");
				Gson gson = new GsonBuilder().create();
				auth.put("LOGIN_STATUS", "success");
				response.getWriter().print(gson.toJson(auth));
			} else {
				response.getWriter().print("username or password is incorrect");
			}
		} else if ("/logout".equals(contentPath)) {
			session.invalidate();
		}
	}

}
