package common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.xml.bind.DatatypeConverter;

public class Database extends HttpServlet {
	private static final long serialVersionUID = -2795916908453809176L;

	protected Connection con;
	protected PreparedStatement pstmt;
	protected ResultSet rs;

	public Database() {

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "ams", "ams");
			// rs = stmt.executeQuery("select * from emp");
			// while (rs.next())
			// System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " +
			// rs.getString(3));

			// step5 close the connection object
			// con.close();

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public String hashPassword(String password) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte[] digest = md.digest();
			return DatatypeConverter.printHexBinary(digest);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public List<Map<String, Object>> query(String sql) {
		logQuery(sql);
		try {
			try {
				pstmt = con.prepareStatement(sql);
				rs = pstmt.executeQuery();
				ResultSetMetaData md = rs.getMetaData();
				int columns = md.getColumnCount();
				ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				while (rs.next()) {
					HashMap<String, Object> row = new HashMap<String, Object>(columns);
					for (int i = 1; i <= columns; ++i) {
						row.put(md.getColumnName(i).toUpperCase(), rs.getObject(i));
					}
					list.add(row);
				}
				return list;
			} finally {
				pstmt.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public List<Map<String, Object>> query(String sql, List<Object> params) {
		logQuery(sql, params);
		try {
			try {
				pstmt = con.prepareStatement(sql);
				for (int idx = 1; idx <= params.size(); idx++) {
					pstmt.setObject(idx, params.get(idx - 1));
				}
				rs = pstmt.executeQuery();
				ResultSetMetaData md = rs.getMetaData();
				int columns = md.getColumnCount();
				ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				while (rs.next()) {
					HashMap<String, Object> row = new HashMap<String, Object>(columns);
					for (int i = 1; i <= columns; ++i) {
						row.put(md.getColumnName(i).toUpperCase(), rs.getObject(i));
					}
					list.add(row);
				}
				return list;
			} finally {
				pstmt.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public Map<String, Object> querySingleRow(String sql) {
		logQuery(sql);
		try {
			try {
				pstmt = con.prepareStatement(sql);
				rs = pstmt.executeQuery();
				ResultSetMetaData md = rs.getMetaData();
				int columns = md.getColumnCount();
				ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				while (rs.next()) {
					HashMap<String, Object> row = new HashMap<String, Object>(columns);
					for (int i = 1; i <= columns; ++i) {
						row.put(md.getColumnName(i).toUpperCase(), rs.getObject(i));
					}
					list.add(row);
				}
				return list != null ? list.get(0) : null;
			} finally {
				pstmt.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public Map<String, Object> querySingleRow(String sql, List<Object> params) {
		logQuery(sql, params);
		try {
			try {
				pstmt = con.prepareStatement(sql);
				for (int idx = 1; idx <= params.size(); idx++) {
					pstmt.setObject(idx, params.get(idx - 1));
				}
				rs = pstmt.executeQuery();
				ResultSetMetaData md = rs.getMetaData();
				int columns = md.getColumnCount();
				ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				while (rs.next()) {
					HashMap<String, Object> row = new HashMap<String, Object>(columns);
					for (int i = 1; i <= columns; ++i) {
						row.put(md.getColumnName(i).toUpperCase(), rs.getObject(i));
					}
					list.add(row);
				}
				return list != null ? list.get(0) : null;
			} finally {
				pstmt.close();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public int executeUpdate(String sql) throws SQLException {
		logQuery(sql);
		try {
			pstmt = con.prepareStatement(sql);
			return pstmt.executeUpdate();
		} finally {
			pstmt.close();
		}
	}

	public int executeUpdate(String sql, List<Object> params) throws SQLException {
		logQuery(sql, params);
		try {
			pstmt = con.prepareStatement(sql);
			for (int idx = 1; idx <= params.size(); idx++) {
				pstmt.setObject(idx, params.get(idx - 1));
			}
			return pstmt.executeUpdate();
		} finally {
			pstmt.close();
		}
	}

	private void logQuery(String sql) {
		logQuery(sql, null);
	}

	private void logQuery(String sql, List<Object> params) {
		System.out.println();
		System.out.println("  Sql : " + sql);
		System.out.println("  Binding paramsmeters : " + params);
	}

}
