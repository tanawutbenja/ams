pageName = 'ams01m';
disabledField = ['FORM_SOFTWARE_TYPE_ID'];

$(document).ready(function(){
	initTable();
	initPage();
	search();
});

function clearCriteria() {
	$('#criteria-form')[0].reset();
}

function search() {
	var form = $('#criteria-form');
// var form = new FormData(jQuery('form')[0]);
	$.ajax({
		type : 'GET',
		url : pageName+"Search",
		data : form.serialize(),
		success : function(data) {
			dataLists = data;
			setActionButton();
			refreshTable();
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function add() {
	currentMode = mode.ADD;
	$("#data-item-form")[0].reset();
	$("#data-item-form").removeClass();
	undisableForm();
	$("#data-item-modal").modal();
}

function edit(index) {
	currentMode = mode.EDIT;
	$("#data-item-form")[0].reset();
	$("#data-item-form").removeClass();
	fillDataForm(dataLists[index]);
	disableForm();
	$("#data-item-modal").modal();
}

function remove(index) {
	currentMode = mode.DELETE;
	$("#data-item-form")[0].reset();
	fillDataForm(dataLists[index]);
	save();
}

function fillDataForm(datas) {
	for(data in datas) {
		$('#FORM_'+data).val(datas[data]);
	}
}

function disableForm() {
	for(field of disabledField) {
		$('#'+field).prop('readonly', true);
	}
}

function undisableForm() {
	for(field of disabledField) {
		$('#'+field).prop('readonly', false);
	}
}

function save() {
	$("#span-mode").html(currentMode.toLowerCase());
	$("#confirm-modal").modal();
}

function saveConfirm() {
	var form = $('#data-item-form');
	$.ajax({
		type : 'POST',
		url : pageName+currentMode,
		data : form.serialize(),
		success : function(data) {
			if(data == "success") {
				showMessage(currentMode+" Successful.");
				if ($("#confirm-modal").hasClass("show"))
					$("#confirm-modal").modal('toggle');
				if ($("#data-item-modal").hasClass("show"))
					$("#data-item-modal").modal('toggle');
				search();
			} else {
				showMessageError(data);
			}
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

// var form = new FormData(jQuery('form')[0]);
// for (var i = 0; i < fileUploads.length; i++) {
// form.append('userfile', fileUploads[i]);
// }
// $
// .ajax({
// type : 'POST',
// url : "checkLeave",
// data : form,
