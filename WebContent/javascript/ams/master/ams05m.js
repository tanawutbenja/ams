pageName = 'ams05m';
disabledField = ['FORM_ID', 'FORM_USERNAME', 'FORM_NAME'];

$(document).ready(function(){
	initTable();
	initPage();
//	$("#data-item-form").bootstrapValidator();
	$('[data-toggle="tooltip"]').tooltip();
	$("#save-form-btn").click(function(event) {
	    var form = $("#data-item-form");
	    var e = $.Event('keyup');
	    e.which = 39;
	    $("#FORM_PASSWORD").trigger(e);
	    if (form[0].checkValidity() === false) {
	      event.preventDefault()
	      event.stopPropagation()
	    }
	    
	    form.addClass('was-validated');
	});
	
	search();
});

$('input[name=CONFIRM_PASSWORD]').keyup(function () {
    if ($('input[name=PASSWORD]').val() === $(this).val()) {
    	$('input[name=PASSWORD]')[0].setCustomValidity('');
    	$('input[name=CONFIRM_PASSWORD]')[0].setCustomValidity('');
    } else {
    	$('input[name=PASSWORD]')[0].setCustomValidity('Passwords must match');
    	$('input[name=CONFIRM_PASSWORD]')[0].setCustomValidity('Passwords must match');
    }
});

$('input[name=PASSWORD]').keyup(function () {
    if ($('input[name=CONFIRM_PASSWORD]').val() === $(this).val()) {
    	$('input[name=PASSWORD]')[0].setCustomValidity('');
    	$('input[name=CONFIRM_PASSWORD]')[0].setCustomValidity('');
    } else {
    	$('input[name=PASSWORD]')[0].setCustomValidity('Passwords must match');
    	$('input[name=CONFIRM_PASSWORD]')[0].setCustomValidity('Passwords must match');
    }
});

function clearCriteria() {
	$('#criteria-form')[0].reset();
}

function search() {
	var form = $('#criteria-form');
	$.ajax({
		type : 'GET',
		url : pageName+"Search",
		data : form.serialize(),
		success : function(data) {
			dataLists = data;
			setActionButton();
			refreshTable();
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function departmentFormatter(value, row) {
	return $("#DEPARTMENT_ID option[value='"+value+"']").text();
}

function positionFormatter(value, row) {
	return $("#POSITION option[value='"+value+"']").text();
}

function add() {
	currentMode = mode.ADD;
	$("#data-item-form")[0].reset();
	$("#data-item-form").removeClass();
	undisableForm();
	$("#data-item-modal").modal();
}

function edit(index) {
	currentMode = mode.EDIT;
	$("#data-item-form")[0].reset();
	$("#data-item-form").removeClass();
	previousData = dataLists[index];
	fillDataForm(dataLists[index]);
	disableForm();
	$("#data-item-modal").modal();
}

function remove(index) {
	currentMode = mode.DELETE;
	$("#data-item-form")[0].reset();
	fillDataForm(dataLists[index]);
	save();
}

function fillDataForm(datas) {
	for(data in datas) {
		$('#FORM_'+data).val(datas[data]);
	}
}

function disableForm() {
	for(field of disabledField) {
		$('#'+field).prop('readonly', true);
	}
}

function undisableForm() {
	for(field of disabledField) {
		$('#'+field).prop('readonly', false);
	}
}

function save() {
	$("#span-mode").html(currentMode.toLowerCase());
	$("#confirm-modal").modal();
}

function saveConfirm() {
	var form = $('#data-item-form');
	$.ajax({
		type : 'POST',
		url : pageName+currentMode,
		data : form.serialize(),
		success : function(data) {
			if(data == "success") {
				showMessage(currentMode+" Successful.");
				if ($("#confirm-modal").hasClass("show"))
					$("#confirm-modal").modal('toggle');
				if ($("#data-item-modal").hasClass("show"))
					$("#data-item-modal").modal('toggle');
				search();
			} else {
				showMessageError(data);
			}
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function hideMode(_mode) {
	if (_mode == currentMode) {
		return "display: none;";
	}
}