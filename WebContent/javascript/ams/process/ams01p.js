pageName = 'ams01p';
disabledField = ['FROM_SOFTWARE_ID', 'FORM_LICENSE_NAME', 'FORM_LICENSE_KEY'];

$(document).ready(function(){
	initTable();
	initPage();
	search();
	autocompleteHardware();
});

function autocompleteHardware() {
	$.ajax({
		type : 'GET',
		url : pageName+"Hardware",
		async : false,
		success : function(data) {
			for (d of data) {
				console.log(d);
			}
			$("#FORM_HARDWARE").autocomplete({
			    source: data
			});
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function clearCriteria() {
	$('#criteria-form')[0].reset();
}

function search() {
	var form = $('#criteria-form');
	$.ajax({
		type : 'GET',
		url : pageName+"Search",
		async : false,
		data : form.serialize(),
		success : function(data) {
			dataLists = data;
			setActionButton();
			refreshTable();
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function statusFormatter(value, row) {
	if (value == "Y") {
		return "<span style=\"color: green\"><i class=\"fas fa-check fa-sm\"></i>&nbsp;available</span>";
	} else {
		return "<span style=\"color: red\"><i class=\"fas fa-times fa-sm\"></i>&nbsp;unusable</span>";
	}
}

function softwareFormatter(value, row) {
	return $("#SOFTWARE_ID option[value='"+value+"']").text();
}

function hardwareChange() {
	$('#FORM_HARDWARE').val($('#FORM_HARDWARE').val().split(":")[0].trim());
}

function add() {
	currentMode = mode.ADD;
	$("#data-item-form")[0].reset();
	$("#data-item-form").removeClass();
	undisableForm();
	$("#data-item-modal").modal();
}

function edit(index) {
	currentMode = mode.EDIT;
	$("#data-item-form")[0].reset();
	$("#data-item-form").removeClass();
	fillDataForm(dataLists[index]);
	disableForm();
	$("#data-item-modal").modal();
}

function remove(index) {
	currentMode = mode.DELETE;
	$("#data-item-form")[0].reset();
	fillDataForm(dataLists[index]);
	save();
}

function fillDataForm(datas) {
	for(data in datas) {
		if (data.indexOf("DATE") != -1) {
			$('#FORM_'+data).val(moment(datas[data]).format('YYYY-MM-DD'));
		} else {
			$('#FORM_'+data).val(datas[data]);
		}
	}
}

function disableForm() {
	for(field of disabledField) {
		if ($('#'+field).is("select")) {
			$('#'+field).prop('disabled', true);
		} else {
			$('#'+field).prop('readonly', true);
		}
	}
}

function undisableForm() {
	for(field of disabledField) {
		if ($('#'+field).is("select")) {
			$('#'+field).prop('disabled', false);
		} else {
			$('#'+field).prop('readonly', false);
		}
	}
}

function save() {
	$("#span-mode").html(currentMode.toLowerCase());
	$("#confirm-modal").modal();
}

function saveConfirm() {
	var form = $('#data-item-form');
	$.ajax({
		type : 'POST',
		url : pageName+currentMode,
		async : false,
		data : form.serialize(),
		success : function(data) {
			if(data == "success") {
				showMessage(currentMode+" Successful.");
				if ($("#confirm-modal").hasClass("show"))
					$("#confirm-modal").modal('toggle');
				if ($("#data-item-modal").hasClass("show"))
					$("#data-item-modal").modal('toggle');
				search();
			} else {
				showMessageError(data);
			}
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}