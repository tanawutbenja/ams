pageName = 'ams03p';
disabledField = ['FROM_SERVICE_TYPE_ID', 'FORM_ASSIGN_TO', 'FORM_DESCRIPTION'];
removeIndex = 0;
detailLists = [];

$(document).ready(function(){
	initTable();
	$('#data-detail-table').bootstrapTable();
	initPage();
	search();
	autocompleteEmployee();
});

function autocompleteEmployee() {
	$.ajax({
		type : 'GET',
		url : pageName+"Employee",
		success : function(data) {
			$("#FORM_ASSIGN_TO").autocomplete({
			    source: data
			});
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function clearCriteria() {
	$('#criteria-form')[0].reset();
}

function search() {
	var form = $('#criteria-form');
	
	$.ajax({
		type : 'GET',
		url : pageName+"Search",
		async : false,
		data : form.serialize(),
		success : function(data) {
			dataLists = data;
			setActionButtonPage();
			refreshTable();
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function setActionButtonPage() {
	for(i=0;i<dataLists.length;i++) {
		var action = "";
		action += '<input type=\"button\" class=\"btn btn-primary\" value=\"Edit\" onclick=\"edit('+i+')\"/>';
		dataLists[i]['ACTION'] = action;
	}
}

function searchDetail() {
	var form = $('#criteria-form');
	
	$.ajax({
		type : 'GET',
		url : pageName+"SearchDetail",
		async : false,
		data : {reqId : $('#FORM_REQ_ORDER_ID').val()},
		success : function(data) {
			detailLists = data;
			refreshDetailTable();
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function setActionButtonDetail(edit, remove) {
	for(i=0;i<detailLists.length;i++) {
		var action = "";
		if (edit) {
			action += '<input type=\"button\" class=\"btn btn-primary\" value=\"Edit\" onclick=\"editDetail('+i+')\"/>';
		}
		if (remove) {
			action += '<input type=\"button\" class=\"btn btn-danger\" value=\"Delete\" onclick=\"removeDetail('+i+')\"/>';
		}
		detailLists[i]['ACTION'] = action;
	}
}

function refreshDetailTable() {
	$('#data-detail-table').bootstrapTable('load', detailLists);
};

function orderTypeFormatter(value, row) {
	console.log(typeof value);
	return $("#"+value.split(":")[0]+" option[value='"+value.split(":")[1]+"']").text();
}

function serviceFormatter(value, row) {
	return $("#SERVICE_TYPE_ID option[value='"+value+"']").text();
}

function employeeFormatter(value, row) {
	return $("#EMPLOYEE_ID option[value='"+value+"']").text();
}

function reqTypeChange() {
	if ($('#FROM_REQ_ORDER_TYPE').val() == "Hardware") {
		$("#Software").addClass("d-none");
		$("#Hardware").removeClass("d-none");
	} else if ($('#FROM_REQ_ORDER_TYPE').val() == "Software") {
		$("#Hardware").addClass("d-none");
		$("#Software").removeClass("d-none");
	}
}

function add() {
	currentMode = mode.ADD;
	$("#data-item-form")[0].reset();
	$("#data-item-form").removeClass();
	undisableForm();
	$("#add-detail").removeClass("d-none");
	detailLists = [];
	refreshDetailTable();
	$("#data-item-modal").modal();
}

function edit(index) {
	currentMode = mode.EDIT;
	$("#data-item-form")[0].reset();
	$("#data-item-form").removeClass();
	fillDataForm(dataLists[index]);
	disableForm();
	$("#add-detail").addClass("d-none");
	searchDetail();
	$("#data-item-modal").modal();
}

function remove(index) {
	currentMode = mode.DELETE;
	$("#data-item-form")[0].reset();
	fillDataForm(dataLists[index]);
	save();
}

function addDetail() {
	var form = $("#data-item-form")

    if (form[0].checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    } else {
    	disableForm();
    	detailLists.push({
    		"REQ_ORDER_TYPE" : $("#FROM_REQ_ORDER_TYPE").val(),
    		"REQ_ORDER_TYPE_ID" : $("#"+$("#FROM_REQ_ORDER_TYPE").val()+" select").val(),
    		"REQ_ORDER_TYPE_ID_COL" : ($("#FROM_REQ_ORDER_TYPE").val())+":"+($("#"+$("#FROM_REQ_ORDER_TYPE").val()+" select").val()),
    		"DESCRIPTION" : $("#FORM_DT_DESCRIPTION").value,
    	});
    	setActionButtonDetail(false, true);
    	refreshDetailTable();
    }
    
    form.addClass('was-validated');
}

function removeDetail(index) {
	removeIndex = index;
	detailLists.pop(index);
	setActionButtonDetail(false, true);
	refreshDetailTable();
}

function fillDataForm(datas) {
	for(data in datas) {
		if (data.indexOf("DATE") != -1) {
			$('#FORM_'+data).val(moment(datas[data]).format('YYYY-MM-DD'));
		} else {
			$('#FORM_'+data).val(datas[data]);
		}
	}
}

function disableForm() {
	for(field of disabledField) {
		if ($('#'+field).is("select")) {
			$('#'+field).prop('disabled', true);
		} else {
			$('#'+field).prop('readonly', true);
		}
	}
}

function undisableForm() {
	for(field of disabledField) {
		if ($('#'+field).is("select")) {
			$('#'+field).prop('disabled', false);
		} else {
			$('#'+field).prop('readonly', false);
		}
	}
}

function approve() {
	currentMode = "Approve";
}

function reject() {
	currentMode = "Reject";
}

function save() {
	$("#span-mode").html(currentMode.toLowerCase());
	$("#confirm-modal").modal();
}

function saveConfirm() {
	var form = $('#data-item-form');
	$.ajax({
		type : 'POST',
		url : pageName+currentMode,
		data : {
			header : JSON.stringify({
				'REQ_ORDER_ID' : $("#FORM_REQ_ORDER_ID").val(),
				'SERVICE_TYPE_ID' : $("#FROM_SERVICE_TYPE_ID").val(),
				'ASSIGN_TO' : $("#FORM_ASSIGN_TO").val(),
				'DESCRIPTION' : $("#FORM_DESCRIPTION").val(),
			}),
			list : JSON.stringify(detailLists)
		},
		success : function(data) {
			if(data == "success") {
				showMessage(currentMode+" Successful.");
				if ($("#confirm-modal").hasClass("show"))
					$("#confirm-modal").modal('toggle');
				if ($("#data-item-modal").hasClass("show"))
					$("#data-item-modal").modal('toggle');
				search();
			} else {
				if ($("#confirm-modal").hasClass("show"))
					$("#confirm-modal").modal('toggle');
				showMessageError(data);
			}
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}