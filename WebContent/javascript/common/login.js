$(document).ready(function() {
	$("#username").val("admin");
	$("#password").val("admin");
});

$('input').keyup(function (e) {
	if (e.keyCode === 13) {
		login();
    }
});

function login() {
	$.ajax({
		type : 'POST',
		url : "login",
		asyns : true,
		data : {
			username : $("#username").val(),
			password : $("#password").val(),
		},
		success : function(data) {
			if ("success" == data['LOGIN_STATUS']) {
				$("#login").html("");
				setCookie("firstName", data.NAME.split(" ")[0]);
				$("#main").load("page/common/main.jsp");
			} else {
				$("#message_error").html(data);
			}
			return false;
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}