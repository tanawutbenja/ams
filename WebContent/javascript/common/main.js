$(document).ready(function(){
	getMenu();
});

var showPage;
var mode = {'ADD':'Add','EDIT':'Edit','DELETE':'Delete'};
var currentMode;

function toggleNav() {
	if ($("#mySidenav").hasClass("short")) $("#mySidenav").removeClass("short");
	else $("#mySidenav").addClass("short");
}

function logout() {
	$("#confirm-logout").modal();
}

function logoutConfirm() {
	$.ajax({
		type : 'POST',
		url : "logout",
		success : function(data) {
			if ($("#confirm-logout").hasClass("show"))
				$("#confirm-logout").modal('toggle');
			$(".modal-backdrop").remove();
			checkSession();
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function gotoPage(page) {
	$('#content').load('page/'+page);
	toggleNav();
}

function getMenu() {
	$.ajax({
		type : 'GET',
		url : "menu",
		asyns : false,
		success : function(data) {
			if ("error" != data) {
				$("#mySidenav").html("<a class=\"navheader\" href=\"javascript:void(0)\" onclick=\"toggleNav()\"><i class=\"fas fa-bars\"></i><span class=\"user-text\">"+getCookie("firstName")+"</span></a>");
				$("#mySidenav").append(data);
				$("#mySidenav").append("<a class=\"navfooter\" href=\"javascript:void(0)\" onclick=\"logout()\"><i class=\"fas fa-sign-out-alt\"></i><span class=\"logout-text\">Logout</span></a>");
			}
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}

function initTable() {
	$('#data-table').bootstrapTable();
};

function initPage() {
	$('#data-item-form').submit(function () {
		setTimeout(function(){}, 500);
		save();
		return false;
	});
	
	$("#save-form-btn").click(function(event) {
	    var form = $("#data-item-form");
	    var e = $.Event('keypress');
	    e.which = 39;
	    $("#data-item-form input[type='text']").trigger(e);
	    if (form[0].checkValidity() === false) {
	      event.preventDefault()
	      event.stopPropagation()
	    }
	    
	    form.addClass('was-validated');
	});
};

function refreshTable() {
	$('#data-table').bootstrapTable('load', dataLists);
};

function showMessage(message) {
	$('#span-message').html(message);
	$('#message-modal').modal();
	setTimeout(function(){
		$('#message-modal').modal('toggle');
	}, 700);
}

function showMessageError(message) {
	$('#message-modal-confirm .modal-body').html(message);
	$('#message-modal-confirm').modal();
}

function closeMessageError() {
	$("#message-modal-confirm").modal('toggle');
}

//function setActionButton() {
//	setActionButton(true, true);
//}

function setActionButton(edit, remove) {
	for(i=0;i<dataLists.length;i++) {
		var action = "";
//		if (edit) {
			action += '<input type=\"button\" class=\"btn btn-primary\" value=\"Edit\" onclick=\"edit('+i+')\"/>';
//		}
//		if (remove) {
			action += '<input type=\"button\" class=\"btn btn-danger\" value=\"Delete\" onclick=\"remove('+i+')\"/>';
//		}
//			a;
		dataLists[i]['ACTION'] = action;
	}
}

function dateFormatter(value, row) {
	return moment(value).format('DD/MM/YYYY');
}

function dateTimeFormatter(value, row) {
	return moment(value).format('DD/MM/YYYY HH:mm:ss');
}

function isValidDate(value) {
    var dateWrapper = new Date(value);
    return !isNaN(dateWrapper.getDate());
}