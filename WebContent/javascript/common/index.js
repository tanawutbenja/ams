$(document).ready(function(){
	checkSession();
//	$("#main").load("page/common/main.jsp");
});

function setCookie (cname, cvalue) {
	var d = new Date();
	d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toGMTString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function checkSession() {
	$.ajax({
		type : 'GET',
		url : "login",
		asyns : true,
		success : function(data) {
			if ("success" == data) {
				$("#login").html("");
				$("#main").load("page/common/main.jsp");
			} else {
				$("#main").html("");
				$("#login").load("page/common/login.jsp");
			}
			return false;
		},
		error : function(data, status, er) {
			console.log("error: " + data + " status: " + status + " er:" + er);
		}
	});
}