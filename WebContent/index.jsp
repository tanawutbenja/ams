<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/css/common/bootstrap.css" />" rel="stylesheet" />
<link href="<c:url value="/css/common/bootstrap-table.min.css" />" rel="stylesheet" />
<link href="<c:url value="/css/common/bootstrapValidator.min.css" />" rel="stylesheet" />
<link href="<c:url value="/css/common/jquery-ui.css" />" rel="stylesheet" />
<link href="<c:url value="/fontawesome/css/all.css" />" rel="stylesheet" />
<title>Asset Management System</title>
</head>
<body style="background: #ebebeb;">
	<script src="<c:url value="/javascript/common/jquery-3.4.1.min.js" />"></script>
	<script src="<c:url value="/javascript/common/Popper.js" />"></script>
	<script src="<c:url value="/javascript/common/moment.js" />"></script>
	<script src="<c:url value="/javascript/common/bootstrap.min.js" />"></script>
	<script src="<c:url value="/javascript/common/bootstrap-table.min.js" />"></script>
	<script src="<c:url value="/javascript/common/bootstrapValidator.min.js" />"></script>
	<script src="<c:url value="/javascript/common/jquery-ui.js" />"></script>
	<script src="<c:url value="/javascript/common/index.js" />"></script>
	<div id="login"></div>
	<div id="main"></div>
</body>
<jsp:include page="page/common/footer.jsp" />
</html>