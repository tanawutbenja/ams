<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<jsp:useBean id="screen" class="process.AMS02P" />
<script src="<c:url value="/javascript/ams/process/ams02p.js" />"></script>
<div id="search-panel">
	<div class="row">
		<div class="col-md-12 mx-auto">
			<form id="criteria-form">
				<div class="form-group row">
					<div class="col-sm-4">
						<label for="SERVICE_TYPE_ID">Service</label>
						<select class="form-control" id="SERVICE_TYPE_ID" name="SERVICE_TYPE_ID">
							<option value="" >All</option>
						    <c:forEach var="item" items="${screen.serviceTypeList}">
						        <option value="${item.key}" >${item.value}</option>
						    </c:forEach>
						</select>
						<select class="d-none form-control" id="EMPLOYEE_ID" name="EMPLOYEE_ID">
						    <c:forEach var="item" items="${screen.employeeList}">
						        <option value="${item.key}" >${item.value}</option>
						    </c:forEach>
						</select>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="search-btn-group">
		<input type="button" value="Add" onclick="add()" class="btn btn-info float-left" />
		<input type="button" value="Search" onclick="search()" class="btn btn-success" />
		<input type="button" value="Clear" onclick="clearCriteria()" class="btn btn-secondary" />
	</div>
</div>
<div id="table-panel">
	<table id="data-table" class="table table-bordered" data-height="470" data-page-list="[5, 10, 25, all]" data-toggle="table" data-pagination="true" data-show-extended-pagination="true" data-page-size="10">
		<thead>
			<tr>
				<th data-field="REQ_ORDER_ID" >ID</th>
				<th data-field="SERVICE_TYPE_ID" data-formatter="serviceFormatter">Service</th>
				<th data-field="DESCRIPTION" >Description</th>
				<th data-field="ASSIGN_TO" data-formatter="employeeFormatter">Assign To</th>
				<th data-field="ACTION" >Action</th>
			</tr>
		</thead>
	</table>
</div>

<!-- Popup -->
<div class="modal fade" id="data-item-modal" role="dialog" tabindex="-1" style="padding-left: 70px">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 mx-auto container">
						<form id="data-item-form" novalidate="">
							<input class="form-control d-none" id="FORM_REQ_ORDER_ID" name="REQ_ORDER_ID" type="text" />
							<div class="form-group row">
								<div class="col-sm-6">
									<label for="SERVICE_TYPE_ID">Service</label>
									<select class="form-control" id="FROM_SERVICE_TYPE_ID" name="SERVICE_TYPE_ID" required>
									    <c:forEach var="item" items="${screen.serviceTypeList}">
									        <option value="${item.key}" >${item.value}</option>
									    </c:forEach>
									</select>
								</div>
								<div class="col-sm-6">
									<label for="ASSIGN_TO">Assign to</label>
									<input class="form-control" id="FORM_ASSIGN_TO" name="ASSIGN_TO" type="text" required/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-12">
									<label for="DESCRIPTION">Description</label>
									<input class="form-control" id="FORM_DESCRIPTION" name="DESCRIPTION" type="text" />
								</div>
							</div>
							<div class="form-group row" id="add-detail">
								<div class="col-md-12 mx-auto container">
									<div class="form-group row" >
										<div class="col-sm-6">
											<label for="REQ_ORDER_TYPE">Service</label>
											<select class="form-control" id="FROM_REQ_ORDER_TYPE" name="REQ_ORDER_TYPE" onchange="reqTypeChange()" required>
										        <option value="Software" selected>Software</option>
										        <option value="Hardware" >Hardware</option>
											</select>
										</div>
										<div class="col-sm-6 d-none" id="Hardware">
											<label for="REQ_ORDER_TYPE_ID">Hardware</label>
											<select class="form-control" name="REQ_ORDER_TYPE_ID" required>
										        <c:forEach var="item" items="${screen.hardwareCategoryList}">
											        <option value="${item.key}" >${item.value}</option>
											    </c:forEach>
											</select>
										</div>
										<div class="col-sm-6" id="Software">
											<label for="REQ_ORDER_TYPE_ID">Software</label>
											<select class="form-control" name="REQ_ORDER_TYPE_ID" required>
										        <c:forEach var="item" items="${screen.softwareList}">
											        <option value="${item.key}" >${item.value}</option>
											    </c:forEach>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12">
											<label for="DT_DESCRIPTION">Description</label>
											<input class="form-control" id="FORM_DT_DESCRIPTION" name="DT_DESCRIPTION" type="text" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12">
											<input type="button" value="Add" onclick="addDetail()" class="btn btn-info float-left" />
										</div>
									</div>
								</div>
							</div>
							<div id="table-detail-panel">
								<table id="data-detail-table" class="table table-bordered"  data-toggle="table" >
									<thead>
										<tr>
											<th data-field="REQ_ORDER_TYPE" >Type</th>
											<th data-field="REQ_ORDER_TYPE_ID_COL" data-formatter="orderTypeFormatter">Request</th>
											<th data-field="ACTION" >Action</th>
										</tr>
									</thead>
								</table>
							</div>
							<div style="height: 5px"></div>
							<div class="float-right" >
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<input type="submit" id="save-form-btn" class="btn btn-primary" value="Save"/>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="confirm-modal" role="dialog" tabindex="-1" style="padding-left: 70px" data-backdrop="false">
	<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Confirm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Do you want to <span id="span-mode"></span> data?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-success" onclick="saveConfirm()">Yes</button>
			</div>
		</div>
	</div>
</div>