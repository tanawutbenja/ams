<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<jsp:useBean id="screen" class="report.AMS01R" />
<script src="<c:url value="/javascript/ams/report/ams01r.js" />"></script>
<div id="search-panel">
	<div class="row">
		<div class="col-md-12 mx-auto">
			<form id="criteria-form">
				<div class="form-group row">
					<div class="col-sm-4">
						<label for="SOFTWARE_ID">Software</label>
						<select class="form-control" id="SOFTWARE_ID" name="SOFTWARE_ID">
							<option value="" >All</option>
						    <c:forEach var="item" items="${screen.softwareList}">
						        <option value="${item.key}" >${item.value}</option>
						    </c:forEach>
						</select>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="search-btn-group">
		<input type="button" value="Search" onclick="search()" class="btn btn-success" />
		<input type="button" value="Clear" onclick="clearCriteria()" class="btn btn-secondary" />
	</div>
</div>
<div id="table-panel">
	<table id="data-table" class="table table-bordered" data-height="470" data-page-list="[5, 10, 25, all]" data-toggle="table" data-pagination="true" data-show-extended-pagination="true" data-page-size="10">
		<thead>
			<tr>
				<th data-field="SOFTWARE_ID" data-formatter="softwareFormatter">Software</th>
				<th data-field="LAST_UPDATE_DATE" data-formatter="dateFormatter">Last Update Date</th>
				<th data-field="PHYSICAL_QTY" >Physical Qty</th>
				<th data-field="AVAILABLE_QTY" >Available Qty</th>
			</tr>
		</thead>
	</table>
</div>