<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<jsp:useBean id="screen" class="report.AMS03R" />
<script src="<c:url value="/javascript/ams/report/ams03r.js" />"></script>
<div id="search-panel">
	<div class="row">
		<div class="col-md-12 mx-auto">
			<form id="criteria-form">
				<div class="form-group row">
					<div class="col-sm-6">
						<label for="SOFTWARE_ID">Software</label>
						<select class="form-control" id="SOFTWARE_ID" name="SOFTWARE_ID">
							<option value="" >All</option>
						    <c:forEach var="item" items="${screen.softwareList}">
						        <option value="${item.key}" >${item.value}</option>
						    </c:forEach>
						</select>
					</div>
					<div class="col-sm-6">
						<label for="STATUS">Status</label>
						<select class="form-control" id="STATUS" name="STATUS">
							<option value="" >All</option>
						    <option value="P" >Pending</option>
						    <option value="A" >Approved</option>
						    <option value="R" >Reject</option>
						</select>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="search-btn-group">
		<button type="button" onclick="print()" class="btn btn-light" style="padding: 10px 20px"><i class="fas fa-print"></i></button>
		<input type="button" value="Search" onclick="search()" class="btn btn-success" />
		<input type="button" value="Clear" onclick="clearCriteria()" class="btn btn-secondary" />
	</div>
</div>
<div id="table-panel">
	<table id="data-table" class="table table-bordered" data-height="470" data-page-list="[5, 10, 25, all]" data-toggle="table" data-pagination="true" data-show-extended-pagination="true" data-page-size="10">
		<thead>
			<tr>
				<th data-field="LICENSE_STATUS" data-formatter="statusFormatter" data-sortable="true">Status</th>
				<th data-field="NAME" >User</th>
				<th data-field="HARDWARE_NAME" >Hardware</th>
				<th data-field="SOFTWARE_ID" data-formatter="softwareFormatter">Software</th>
				<th data-field="LICENSE_KEY" >License</th>
				<th data-field="PURCH_DATE" data-formatter="dateFormatter">Installed Date</th>
				<th data-field="REMAIN" >Remain (Day)</th>
				<th data-field="EXP_DATE" data-formatter="dateFormatter">Expire Date</th>
			</tr>
		</thead>
	</table>
</div>