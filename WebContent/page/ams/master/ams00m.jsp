<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="page12" class="ams.AMS00M" />
<script src="<c:url value="/javascript/ams/master/ams00m.js" />"></script>
<div id="search-panel">
	<div class="row">
        <div class="col-md-6 form-group">
            <label>Label1</label>
            <input class="form-control" type="text" value="${page12.criteria['LABEL1']}" />
        </div>
        <div class="col-md-6 form-group">
            <label>Label2</label>
            <input class="form-control" type="text"/>
        </div>
    </div>
    <div id="search-btn-group">
		<input type="button" value="Search" class="btn btn-success" />
		<input type="button" value="${page12.test}" onclick="#{page12.testt()}" class="btn btn-secondary" />
	</div>
</div>
<div id="table-panel">
	<table id="data-table" class="table table-bordered" data-height="470" data-page-list="[5, 10, 25, all]" data-toggle="table" data-pagination="true" data-show-extended-pagination="true" data-page-size="10">
        <thead>
            <tr>
                <th data-field="id" >Item ID</th>
                <th data-field="name" >Item Name</th>
                <th data-field="price" >Item Price</th>
            </tr>
        </thead>
    </table>
</div>