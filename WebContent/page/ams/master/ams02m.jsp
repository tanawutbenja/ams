<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<jsp:useBean id="screen" class="master.AMS02M" />
<script src="<c:url value="/javascript/ams/master/ams02m.js" />"></script>
<div id="search-panel">
	<div class="row">
		<div class="col-md-12 mx-auto">
			<form id="criteria-form">
				<div class="form-group row">
					<div class="col-sm-4">
						<label for="SOFTWARE_ID">ID</label>
						<input class="form-control" id="SOFTWARE_ID" name="SOFTWARE_ID" type="text" />
					</div>
					<div class="col-sm-4">
						<label for="SOFTWARE_NAME">Software Name</label>
						<input class="form-control" id="SOFTWARE_NAME" name="SOFTWARE_NAME" type="text" />
					</div>
					<div class="col-sm-4">
						<label for="SOFTWARE_TYPE_ID">Software Type</label>
						<select class="form-control" id="SOFTWARE_TYPE_ID" name="SOFTWARE_TYPE_ID" >
							<option value="" >All</option>
						    <c:forEach var="item" items="${screen.softwareTypeList}">
						        <option value="${item.key}" >${item.value}</option>
						    </c:forEach>
						</select>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="search-btn-group">
		<input type="button" value="Add" onclick="add()" class="btn btn-info float-left" />
		<input type="button" value="Search" onclick="search()" class="btn btn-success" />
		<input type="button" value="Clear" onclick="clearCriteria()" class="btn btn-secondary" />
	</div>
</div>
<div id="table-panel">
	<table id="data-table" class="table table-bordered" data-height="470" data-page-list="[5, 10, 25, all]" data-toggle="table" data-pagination="true" data-show-extended-pagination="true" data-page-size="10">
		<thead>
			<tr>
				<th data-field="SOFTWARE_ID" >ID</th>
				<th data-field="SOFTWARE_NAME" >Software Name</th>
				<th data-field="SOFTWARE_TYPE_ID" data-formatter="swTypeFormatter" data-sortable="true">Software Type</th>
				<th data-field="DESCRIPTION" >Description</th>
				<th data-field="ACTION" >Action</th>
			</tr>
		</thead>
	</table>
</div>

<!-- Popup -->
<div class="modal fade" id="data-item-modal" role="dialog" tabindex="-1" style="padding-left: 70px">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 mx-auto container">
						<form id="data-item-form" novalidate="">
							<input class="form-control d-none" id="FORM_SOFTWARE_ID" name="SOFTWARE_ID" type="text"/>
							<div class="form-group row">
								<div class="col-sm-6">
									<label for="SOFTWARE_NAME">Software Name</label>
									<input class="form-control" id="FORM_SOFTWARE_NAME" name="SOFTWARE_NAME" type="text" required/>
								</div>
								<div class="col-sm-6">
									<label for="SOFTWARE_TYPE_ID">Software Type</label>
									<select class="form-control" id="FORM_SOFTWARE_TYPE_ID" name="SOFTWARE_TYPE_ID" required>
									    <c:forEach var="item" items="${screen.softwareTypeList}">
									        <option value="${item.key}" >${item.value}</option>
									    </c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-12">
									<label for="DESCRIPTION">Description</label>
									<input class="form-control" id="FORM_DESCRIPTION" name="DESCRIPTION" type="text" />
								</div>
							</div>
							<div class="float-right">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<input type="submit" id="save-form-btn" class="btn btn-primary" value="Save"/>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="confirm-modal" role="dialog" tabindex="-1" style="padding-left: 70px" data-backdrop="false">
	<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Confirm</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				Do you want to <span id="span-mode"></span> data?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-success" onclick="saveConfirm()">Yes</button>
			</div>
		</div>
	</div>
</div>