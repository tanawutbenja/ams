<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background: #FFFFFF;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  //font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form .login-btn {
  //font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #4CAF50;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form .login-btn:hover {
  background: #43A047;
}
#message_error {
  height: 25px;
  font-size: 14px;
  color: #d42a2a;
}
</style>
<div class="login-page">
	<script src="<c:url value="/javascript/common/login.js" />"></script>
  	<div class="form">
		<form class="login-form">
			<input id="username" type="text" value="admin" placeholder="username"/>
			<input id="password" type="password" value="admin" placeholder="password"/>
			<div id="message_error"></div>
			<input class="login-btn" type="button" value="LOGIN" onclick="login()" />
		</form>
  	</div>
</div>