<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/css/common/main.css" />" rel="stylesheet" />
<div>
	<script src="<c:url value="/javascript/common/main.js" />"></script>
	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" onclick="toggleNav()"><i class="fas fa-bars"></i></a>
	</div>
	<div id="content" class="content">
		<jsp:include page="../ams/begin.jsp" />
	</div>
</div>
<div class="modal fade" id="confirm-logout" role="dialog" tabindex="-1" style="padding-left: 70px" >
	<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				Are you sure you want to log out?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
				<button type="button" class="btn btn-success" onclick="logoutConfirm()">Yes</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="message-modal" role="dialog" tabindex="-1" style="padding-left: 70px" data-backdrop="static">
	<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<span id="span-message"></span>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="message-modal-confirm" role="dialog" tabindex="-1" style="padding-left: 70px" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" onclick="closeMessageError()">Close</button>
			</div>
		</div>
	</div>
</div>